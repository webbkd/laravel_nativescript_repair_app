<?php

namespace App\Http\Controllers;

use App\repair;
use Illuminate\Http\Request;

class RepairController extends Controller
{
    //

    public function index(){
        $repairs = repair::all();

        return view('repair.index', compact('repairs'));
    }

    public function createRepairs(){
        return view('repair.createRepair');
    }

    public function storeRepair(Request $request){
        
        $this->validate(request(),[
            'firstName'=>'required',
            'lastName'=>'required',
            'phoneNumber'=>'required',
            'device'=>'required',
            'serialNumber'=>'required',
            'repairType'=>'required',
            'price'=>'required'
        ]);

        $repair = new repair;

        repair::create(request(['firstName','lastName','email','phoneNumber','device','serialNumber','repairType','price']));

        return redirect('/');
    }

    public function editRepair($id){
        $repair = repair::find($id);

        return view('repair.editRepair', compact('repair'));
    }

    public function updateRepair(Request $request, $id){

        $repair = repair::find($id);

        $repair->firstName = $request->firstName;
        $repair->lastName = $request->lastName;
        $repair->email = $request->email;
        $repair->phoneNumber = $request->phoneNumber;
        $repair->device = $request->device;
        $repair->serialNumber = $request->serialNumber;
        $repair->repairType = $request->repairType;
        $repair->completed = $request->completed;
        $repair->price = $request->price;

        $repair->save();

        return redirect('/');
    }


    public function storeRepairApi(Request $request){

        $repair = new Repair([
            'firstName'=> $request->get('firstName'),
            'lastName'=>$request->get('lastName'),
            'email'=>$request->get('email'),
            'phoneNumber'=>$request->get('phoneNumber'),
            'device'=>$request->get('device'),
            'serialNumber'=>$request->get('serialNumber'),
            'repairType'=>$request->get('repairType')
        ]);
 
        $repair->save();
        return response()->json('Successfully added');   
    }

    public function destroy(Request $request, $id){
        $repair = repair::find($id);
        
        $repair->delete();

        return redirect('/');
        
    }
}
