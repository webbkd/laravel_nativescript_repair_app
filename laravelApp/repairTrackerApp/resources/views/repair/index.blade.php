@extends('layouts.master')


@section ('content')
    <div class="row center">
        <h2>Your recent repairs</h2>
    </div>
    <div class="container">
        <div class="card">
            <div class="card-content">
                <table>
                    <thead>
                    <tr>
                            <th>Date</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Device</th>
                            <th>Serial Number</th>
                            <th>Repair Type</th>
                            <th>Price</th>
                            <th>Completed?</th>
                            <th colspan="1"></th>
                    </tr>
                    </thead>

                    <tbody>
                            @foreach($repairs as $repair)
                            <tr>
                            <td>{{ $repair->created_at }}</td>
                            <td>{{ $repair->firstName }}</td>
                            <td>{{ $repair->lastName }}</td>
                            <td>{{ $repair->email }}</td>
                            <td>{{ $repair->phoneNumber }}</td>
                            <td>{{ $repair->device }}</td>
                            <td>{{ $repair->serialNumber }}</td>
                            <td>{{ $repair->repairType }}</td>
                            <td>{{ $repair->price }}</td>
                            <td>{{ $repair->completed }}</td>
                            <td><a href="/editRepair/{{ $repair->id }}" class="btn btn-primary">Update</a></td>
                            <td><a href="/destroyRepair/{{ $repair->id }}" class="btn red">Delete</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-action">
                <a class="btn" href="/createRepairs">Add a new Repair</a>
            </div>
        </div>
    </div>
@endsection