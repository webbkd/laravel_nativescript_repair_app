@extends('layouts.master')



@section('content')

<div class="container">
    <h2>Repair ID: {{$repair->id}}</h2>
    <form method="POST" action="/updateRepair/{{$repair->id}}">
        {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s6">
                  <input id="first_name" type="text" name="firstName" class="validate" value="{{$repair->firstName}}">
                  <label for="first_name">First Name</label>
                </div>
                <div class="input-field col s6">
                  <input id="last_name" type="text" name="lastName" class="validate" value="{{$repair->lastName}}">
                  <label for="last_name">Last Name</label>
                </div>
            </div>

              <div class="row">
                <div class="input-field col s6">
                  <input id="phoneNumber" type="text" name="phoneNumber" class="validate"value="{{$repair->phoneNumber}}">
                  <label for="phoneNumber">Phone Number</label>
                </div>

                <div class="input-field col s6">
                    <input id="email" type="email" name="email" class="validate" value="{{$repair->email}}">
                    <label for="email">Email</label>
                </div>
              </div>



              <div class="row">
                <div class="input-field col s6">
                    <input id="device" type="text" name="device" class="validate" value="{{$repair->device}}">
                    <label for="device">Device</label>
                </div>
                <div class="input-field col s6">
                    <input id="serialNumber" type="text" name="serialNumber" class="validate" value="{{$repair->serialNumber}}">
                    <label for="serialNumber">Serial Number</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s6">
                    <input id="repair" type="text" name="repairType" class="validate" value="{{$repair->repairType}}">
                    <label for="repair">Repair Type</label>
                </div>
                <div class="input-field col s6">
                    <input id="price" type="text" name="price" class="validate" value="{{$repair->price}}">
                    <label for="price">Price</label>
                </div>
                <div class="input-field col s12">
                    <select id="completed" name="completed">
                        <option value="Incomplete">Incomplete</option>
                        <option value="Complete">Complete</option>
                        <option value="Returned Not Fixed">Returned Not Fixed</option>
                    </select>
                    <label>Is Completed?</label>
                </div>
               </div>
               <button class="btn waves-effect waves-light" type="submit" name="action">Submit</button>
    </form>
</div>

@endsection