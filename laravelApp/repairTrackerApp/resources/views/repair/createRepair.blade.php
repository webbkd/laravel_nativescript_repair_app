@extends('layouts.master')



@section('content')

<div class="container">
    <form method="POST" action="/addRepair">
        {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s6">
                  <input id="first_name" type="text" name="firstName" class="validate">
                  <label for="first_name">First Name</label>
                </div>
                <div class="input-field col s6">
                  <input id="last_name" type="text" name="lastName" class="validate">
                  <label for="last_name">Last Name</label>
                </div>
            </div>

              <div class="row">
                <div class="input-field col s6">
                  <input id="phoneNumber" type="text" name="phoneNumber" class="validate">
                  <label for="phoneNumber">Phone Number</label>
                </div>

                <div class="input-field col s6">
                    <input id="email" type="email" name="email" class="validate">
                    <label for="email">Email</label>
                </div>
              </div>



              <div class="row">
                <div class="input-field col s6">
                    <input id="device" type="text" name="device" class="validate">
                    <label for="device">Device</label>
                </div>
                <div class="input-field col s6">
                    <input id="serialNumber" type="text" name="serialNumber" class="validate">
                    <label for="serialNumber">Serial Number</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s6">
                    <input id="repair" type="text" name="repairType" class="validate">
                    <label for="repair">Repair Type</label>
                </div>
                <div class="input-field col s6">
                    <input id="price" type="text" name="price" class="validate">
                    <label for="price">Price</label>
                </div>
               </div>
               <button class="btn waves-effect waves-light" type="submit" name="action">Submit</button>
    </form>
    
</div>

@endsection