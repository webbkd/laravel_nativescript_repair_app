<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'RepairController@index')->name('home');

Route::get('/displayRepairs','RepairController@displayRepairs');

Route::get('/createRepairs','RepairController@createRepairs');

Route::post('/addRepair','RepairController@storeRepair');

Route::get('/editRepair/{repair}', 'RepairController@editRepair');

Route::post('/updateRepair/{repair}', 'RepairController@updateRepair');

Route::get('/destroyRepair/{repair}', 'RepairController@destroy');
